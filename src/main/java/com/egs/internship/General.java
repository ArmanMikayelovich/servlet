package com.egs.internship;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class General extends HttpServlet {
    //User list
    public static Map<String, String> userMap = new ConcurrentHashMap<>();

    {
        userMap.put("root", "root");
        userMap.put("admin", "admin");
        userMap.put("manager", "manager");
        userMap.put("valodik", "valodik");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req);

    }
}
